import json
import os

# 创建一个空的列表用于存储所有的记录
records = []
image_dir = '/data/csxjiang/ood_data/zoc_all'
imgs = os.listdir(image_dir)
image_name_to_idx = dict()
# import random
# imgs = random.sample(imgs, k=100)

for idx, img in enumerate(imgs):
    # 创建一个字典来存储每一条记录的数据
    record = {
        "question_id": idx,
        "image": img,
        "text": "Give the most important entity's name. (the name is limted to three words)",
        "category": "detail"
    }
    # 将字典添加到列表中
    records.append(record)
    image_name_to_idx[img] = idx

# 打开一个新的JSONL文件以写入模式
with open('qa_questions_zoc_all.jsonl', 'w') as outfile:
    for record in records:
        # 将字典转换为JSON字符串并写入文件
        # 同时在每条记录后添加一个换行符，以符合JSONL的格式
        json_record = json.dumps(record)
        outfile.write(json_record + '\n')

json.dump(image_name_to_idx, open("image_name_to_idx.json", "w"))
