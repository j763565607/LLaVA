import json

# 创建一个空的列表用于存储所有的记录
records = []

# 使用for循环从1到999生成数据
# for i in range(1, 1000):
#     # 创建一个字典来存储每一条记录的数据
#     record = {
#         "question_id": i,
#         "image": f"{i}.png",
#         "text": "What is the letter in the image? Please answer in one word.",
#         "category": "detail"
#     }
#     # 将字典添加到列表中
#     records.append(record)

# import string
# letter1 = list(string.ascii_uppercase)
# letter2 = list(string.ascii_lowercase)
#
# for item in letter1:
#     record = {
#             "question_id": item,
#             "image": f"{item}.png",
#             "text": "What is the letter in the image? Please answer in one word.",
#             "category": "detail"
#         }
#     records.append(record)
#
# for item in letter2:
#     record = {
#             "question_id": item,
#             "image": f"{item}.png",
#             "text": "What is the letter in the image? Please answer in one word.",
#             "category": "detail"
#         }
#     records.append(record)

with open('common_word.txt', 'r') as f:
    lines = f.readlines()
    for line in lines:
        line = line.strip()
        record = {
                "question_id": line,
                "image": f"{line}.png",
                "text": "What is the word in the image? Please answer in one word.",
                "category": "detail"
            }
        records.append(record)


# 打开一个新的JSONL文件以写入模式
with open('qa_questions_word.jsonl', 'w') as outfile:
    for record in records:
        # 将字典转换为JSON字符串并写入文件
        # 同时在每条记录后添加一个换行符，以符合JSONL的格式
        json_record = json.dumps(record)
        outfile.write(json_record + '\n')
