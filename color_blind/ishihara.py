import math
import random
import sys

from PIL import Image, ImageDraw

# Try to import KDTree for efficient spatial indexing and numpy for numerical operations
# If unavailable, the script will fall back to a less efficient method
try:
    from scipy.spatial import KDTree as KDTree
    import numpy as np
    IMPORTED_SCIPY = True
except ImportError:
    IMPORTED_SCIPY = False

# Set a constant for the background color as white
BACKGROUND = (255, 255, 255)
# Define the total number of circles to be generated
TOTAL_CIRCLES = 1500

# Function to convert hex color to RGB
def hex_to_rgb(hex_color):
    hex_color = hex_color.lstrip('#')
    return tuple(int(hex_color[i:i+2], 16) for i in (0, 2, 4))

# Define multiple color schemes
PALETTES = {
    'General 1': {
        'colors_on': [hex_to_rgb('#F9BB82'), hex_to_rgb('#EBA170'), hex_to_rgb('#FCCD84')],
        'colors_off': [hex_to_rgb('#9CA594'), hex_to_rgb('#ACB4A5'), hex_to_rgb('#BBB964'),
                       hex_to_rgb('#D7DAAA'), hex_to_rgb('#E5D57D'), hex_to_rgb('#D1D6AF')]
    },
    'General 2': {
        'colors_on': [hex_to_rgb('#89B270'), hex_to_rgb('#7AA45E'), hex_to_rgb('#B6C674'),
                      hex_to_rgb('#7AA45E'), hex_to_rgb('#B6C674')],
        'colors_off': [hex_to_rgb('#F49427'), hex_to_rgb('#C9785D'), hex_to_rgb('#E88C6A'),
                       hex_to_rgb('#F1B081')]
    },
    'General 3': {
        'colors_on': [hex_to_rgb('#89B270'), hex_to_rgb('#7AA45E'), hex_to_rgb('#B6C674'),
                      hex_to_rgb('#7AA45E'), hex_to_rgb('#B6C674'), hex_to_rgb('#FECB05')],
        'colors_off': [hex_to_rgb('#F49427'), hex_to_rgb('#C9785D'), hex_to_rgb('#E88C6A'),
                       hex_to_rgb('#F1B081'), hex_to_rgb('#FFCE00')]
    },
    'Protanopia': {
        'colors_on': [hex_to_rgb('#E96B6C'), hex_to_rgb('#F7989C')],
        'colors_off': [hex_to_rgb('#635A4A'), hex_to_rgb('#817865'), hex_to_rgb('#9C9C84')]
    },
    'Protanomaly': {
        'colors_on': [hex_to_rgb('#AD5277'), hex_to_rgb('#F7989C')],
        'colors_off': [hex_to_rgb('#635A4A'), hex_to_rgb('#817865'), hex_to_rgb('#9C9C84')]
    },
    'Viewable by all': {
        'colors_on': [hex_to_rgb('#FF934F')],
        'colors_off': [hex_to_rgb('#9C9C9C')]
    },
    'Colorblind only': {
        'colors_on': [hex_to_rgb('#A8AA00'), hex_to_rgb('#83BE28')],
        'colors_off': [hex_to_rgb('#828200'), hex_to_rgb('#669A1B'), hex_to_rgb('#828200'),
                       hex_to_rgb('#669A1B'), hex_to_rgb('#ED6311')]
    }
}


# Select a color scheme by name
current_palette = 'General 1'  # Change to your preferred palette

def generate_circle(image_width, image_height, min_diameter, max_diameter):
    radius = random.triangular(min_diameter, max_diameter,
                               max_diameter * 0.8 + min_diameter * 0.2) / 2

    angle = random.uniform(0, math.pi * 2)
    distance_from_center = random.uniform(0, image_width * 0.48 - radius)
    x = image_width  * 0.5 + math.cos(angle) * distance_from_center
    y = image_height * 0.5 + math.sin(angle) * distance_from_center

    return x, y, radius


def overlaps_motive(image, circle):
    x, y, r = circle
    points_x = [x, x, x, x-r, x+r, x-r*0.93, x-r*0.93, x+r*0.93, x+r*0.93]
    points_y = [y, y-r, y+r, y, y, y+r*0.93, y-r*0.93, y+r*0.93, y-r*0.93]

    for xy in zip(points_x, points_y):
        if image.getpixel(xy)[:3] != BACKGROUND:
            return True

    return False


def circle_intersection(circle1, circle2):
    x1, y1, r1 = circle1
    x2, y2, r2 = circle2
    return (x2 - x1)**2 + (y2 - y1)**2 < (r2 + r1)**2



# Function to draw a circle on the image
def circle_draw(draw_image, image, circle):
    # Choose the color palette based on overlap with the motive
    fill_colors = PALETTES[current_palette]['colors_on'] if overlaps_motive(image, circle) else PALETTES[current_palette]['colors_off']
    fill_color = random.choice(fill_colors)

    x, y, r = circle
    # Draw the circle on the image
    draw_image.ellipse((x - r, y - r, x + r, y + r),
                       fill=fill_color,
                       outline=fill_color)

# Main function to execute the generation and drawing of circles
def main():
    # Check if a color scheme argument is provided
    if len(sys.argv) > 2:
        global current_palette
        current_palette = sys.argv[2]
        if current_palette not in PALETTES:
            print(f"Color scheme '{current_palette}' is not defined. Using default.")
            current_palette = 'General 1'

    image_path = sys.argv[1] if len(sys.argv) > 1 else 'image.jpg'  # Default image path
    image = Image.open(image_path).convert('RGB')
    image2 = Image.new('RGB', image.size, BACKGROUND)
    draw_image = ImageDraw.Draw(image2)

    width, height = image.size
    min_diameter = (width + height) / 200
    max_diameter = (width + height) / 75
    circles = []

    for i in range(TOTAL_CIRCLES):
        circle = generate_circle(width, height, min_diameter, max_diameter)
        if IMPORTED_SCIPY and circles:
            kdtree = KDTree([(x, y) for (x, y, _) in circles])
            _, indexes = kdtree.query([(circle[0], circle[1])], k=12)
            if not any(circle_intersection(circle, circles[index]) for index in indexes if index < len(circles)):
                circles.append(circle)
                circle_draw(draw_image, image, circle)
        else:
            if not any(circle_intersection(circle, c) for c in circles):
                circles.append(circle)
                circle_draw(draw_image, image, circle)

        # Print progress information
        print(f'{i + 1}/{TOTAL_CIRCLES} circles drawn.', end='\r', flush=True)

    # Show the final image with all circles drawn
    # image2.show()
    output_path = "/data/csxjiang/color_blind/ishihara/output_image.png"  # Specify the output file path and name
    image2.save(output_path)

if __name__ == '__main__':
    main()
