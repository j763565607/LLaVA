#python ./llava/eval/model_vqa.py \
#    --model-path liuhaotian/llava-v1.5-13b \
#    --question-file \
#    playground/data/color_blind/qa_questions_letter.jsonl \
#    --image-folder \
#    /data/csxjiang/color_blind/ishihara_letter/general1 \
#    --answers-file \
#    ./results/answer-file-ishihara_letter_general1.jsonl


#python ./llava/eval/model_vqa.py \
#    --model-path liuhaotian/llava-v1.5-13b \
#    --question-file \
#    playground/data/zoc/qa_questions_zoc_all.jsonl \
#    --image-folder \
#    /data/csxjiang/ood_data/zoc_all \
#    --answers-file \
#    ./results/answer-file-zoc-all.jsonl

#!/bin/bash

# 指定要遍历的文件夹路径
main_folder="/data/csxjiang/color_blind/ishihara_utils/outputs"

# 使用for循环遍历主文件夹下的所有子文件夹
for subfolder in "$main_folder"/*/; do
    # style
    subfolder_name=$(basename "$subfolder")
    # hsv
    for sub_subfolder in "$subfolder"/*/; do
        # modify
        sub_subfolder_name=$(basename "$sub_subfolder")
        # 使用for循环遍历子子文件夹下的所有文件夹
        for sub_sub_subfolder in "$sub_subfolder"/*/; do
            # 提取子子子文件夹名称并输出
            sub_sub_subfolder_name=$(basename "$sub_sub_subfolder")

            python ./llava/eval/model_vqa.py \
            --model-path liuhaotian/llava-v1.5-13b \
            --question-file \
            playground/data/color_blind/qa_questions_number.jsonl \
            --image-folder \
            "$main_folder/$subfolder_name/$sub_subfolder_name/$sub_sub_subfolder_name" \
            --answers-file \
            "./results/modify_res/$subfolder_name/$sub_subfolder_name/${sub_sub_subfolder_name//\//}.jsonl"
        done
    done
done
