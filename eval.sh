
OPENAI_API_KEY="393f2ed9-385f-43b8-aa69-7c8fcf9b5214" python llava/eval/eval_gpt_review_visual.py \
    --question playground/data/coco2014_val_qa_eval/qa90_questions.jsonl \
    --context llava/eval/table/caps_boxes_coco2014_val_80.jsonl \
    --answer-list \
    playground/data/coco2014_val_qa_eval/qa90_gpt4_answer.jsonl \
    ./results/answer-file-our.jsonl \
    --rule llava/eval/table/rule.json \
    --output ./results/review.json
